. .env
for library in $(find $MAIN_PATH -type d -name "__bin")
do
    for elt in $(find "$library" -type f)
    do
        chmod +x "$elt"
    done
done
