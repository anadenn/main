. .env
for library in $(find $MAIN_PATH -type d -name ".git")
do
    cd $(dirname $library)
    git pull
done
