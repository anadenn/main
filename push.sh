. .env
DEBUG true

for library in $(find $MAIN_PATH -type d -name ".git")
do
    TITLE $(dirname $library)
    cd $(dirname $library)
    git add -A
    git commit -m "automatic add"
    git push
done
