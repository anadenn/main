export MAIN_PATH=$(realpath $(dirname $0)/..)
root_url="git@framagit.org:anadenn"


function GIT(){

    if [ ! -d "$MAIN_PATH/$1" ]
    then
        git clone $root_url/$1.git $MAIN_PATH/$1


        cd $MAIN_PATH/$1
        git switch --create main
        touch README.md
        git add README.md
        git commit -m "add README"
        git push --set-upstream origin main
    fi

    cp "$MAIN_PATH/main/__etc/gitignore" "$MAIN_PATH/$1/.gitignore"

}

GIT "main"

GIT "core/bash.script"

GIT "core/python"
GIT "core/python.model"
GIT "core/python.tree"
GIT "core/python.tree.http"

GIT "core/python.gtk"
GIT "core/python.flask"
GIT "core/python.matplot"

GIT "apps/debian"
GIT "apps/gnome"
GIT "apps/arduino"
