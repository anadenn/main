
function LOCALHOST(){

    export SERVER="localhost"
    LINE "SERVER=$SERVER"

}
function SSHSERVER(){
    if [ "$(hostname)" == "$1" ]
    then
        export SERVER="localhost"
    else
        export SERVER="$1"
    fi

    LINE "SERVER=$SERVER"

}

function CMD(){
    LINE "CMD $SERVER : $@"
    if [ "$SERVER" == "localhost" ]
    then
        $@
    else
        ssh $SERVER $@
    fi
}

function SUDO(){
    CMD sudo $@
}
