

function DEBIAN(){
    APT_CLEAN

    APT "lsb-release"
    APT "apt-transport-https"
    APT "ca-certificates"

    APT "aptitude"
    APT "tree"

}

function APT(){
    SUDO apt-get install -y $@
}
function APT_RM(){
    SUDO apt-get purge -y $@
}

function APT_CLEAN(){
    SUDO apt-get update
    SUDO apt-get upgrade
    SUDO apt-get dist-upgrade -y
}
