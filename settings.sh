export MAIN_PATH=$(realpath $(dirname $0)/..)

echo "export MAIN_PATH=$MAIN_PATH" > .env

for library in $(find $MAIN_PATH -type d -name "__bin")
do
    echo "export PATH=$library:\$PATH" >> .env
done

for library in $(find $MAIN_PATH -type d -name "__bash")
do

    for elt in $(find "$library" -mindepth 1 -type f -name "*.sh")
    do
        #echo $elt
        echo "source $elt" >> .env
    done

done
    echo "export PYTHONPATH=\"\"" >> .env
for library in $(find $MAIN_PATH -type d -name "__python3")
do
    echo "export PYTHONPATH=$library:\$PYTHONPATH" >> .env
done
